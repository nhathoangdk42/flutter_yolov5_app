import 'dart:math';

import 'package:flutter/material.dart';

const List<String> displayLabels = ['product', 'unknow'];

class Recognition implements Comparable<Recognition> {
  Recognition(this._id, this._labelId, this._score, this._location, this._angle,
      this._image);
  final int _id;
  int get id => _id;
  String _labelId;
  String get label => _labelId;
  String get displayLabel => _labelId;
  set setLabel(String name) {
    _labelId = name;
  }

  final double _score;
  double get score => _score;
  final Rect _location;
  Rect get location => _location;
  final int _angle;
  int get angle => _angle;

  Image _image;
  Image get image => _image;
  set setImage(Image image) {
    _image = image;
  }

  Rect getRenderLocation(Size actualPreviewSize, double pixelRatio) {
    final ratioX = pixelRatio;
    final ratioY = ratioX;

    final transLeft = max(0.1, location.left * ratioX);
    final transTop = max(0.1, location.top * ratioY);
    final transWidth = min(
      location.width * ratioX,
      actualPreviewSize.width,
    );
    final transHeight = min(
      location.height * ratioY,
      actualPreviewSize.height,
    );
    final transformedRect =
        Rect.fromLTWH(transLeft, transTop, transWidth, transHeight);
    return transformedRect;
  }

  @override
  int compareTo(Recognition other) {
    if (this.score == other.score) {
      return 0;
    } else if (this.score > other.score) {
      return -1;
    } else {
      return 1;
    }
  }
}
