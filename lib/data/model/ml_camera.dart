import 'dart:convert';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_yolov5_app/main.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:image/image.dart' as image_lib;
import 'package:tflite_flutter/tflite_flutter.dart';

import 'package:flutter_yolov5_app/data/model/classifier.dart';
import 'package:flutter_yolov5_app/utils/image_utils.dart';
import 'package:flutter_yolov5_app/data/entity/recognition.dart';
import 'package:path_provider/path_provider.dart';

final recognitionsProvider = StateProvider<List<Recognition>>((ref) => []);

final mlCameraProvider =
    FutureProvider.autoDispose.family<MLCamera, Size>((ref, size) async {
  final cameras = await availableCameras();
  final cameraController = CameraController(
    cameras[0],
    ResolutionPreset.low,
    enableAudio: false,
  );
  await cameraController.initialize();
  final mlCamera = MLCamera(
    ref.read,
    cameraController,
    size,
  );
  return mlCamera;
});

class MLCamera {
  MLCamera(
    this._read,
    this.cameraController,
    this.cameraViewSize,
  ) {
    Future(() async {
      classifier = Classifier();
      loadVector();
      await cameraController.startImageStream(onCameraAvailable);
    });
  }
  loadVector() async {
    var jsonText = await rootBundle.loadString('assets/vectors.json');
    data = json.decode(jsonText);
  }

  Map data = {};

  final Reader _read;
  final CameraController cameraController;

  final Size cameraViewSize;

  late double ratio = Platform.isAndroid
      ? cameraViewSize.width / cameraController.value.previewSize!.height
      : cameraViewSize.width / cameraController.value.previewSize!.width;

  late Size actualPreviewSize = Size(
    cameraViewSize.width,
    cameraViewSize.width * ratio,
  );

  static late Classifier classifier;

  bool isPredicting = false;

  var count = 0;

  Future<void> onCameraAvailable(CameraImage cameraImage) async {
    if (classifier.interpreter == null) {
      return;
    }

    if (isPredicting) {
      return;
    }

    isPredicting = true;
    final isolateCamImgData = IsolateData(
        cameraImage: cameraImage,
        interpreterAddress: classifier.interpreter!.address,
        interpreterAddress2: classifier.interpreter2!.address);
    print("Frame - w:${cameraImage.width} h:${cameraImage.height}");
    Map map = {"img": isolateCamImgData, "data": data};
    if (count % 3 == 0) {
      var now = new DateTime.now().millisecondsSinceEpoch;
      _read(recognitionsProvider.notifier).state =
          await compute(inference, map);
      var now1 = new DateTime.now().millisecondsSinceEpoch;
      print("Time tong:${now1 - now}");
      isPredicting = false;
    }
    isPredicting = false;
    count++;
  }

  /// inference function
  static Future<List<Recognition>> inference(Map a) async {
    var image = ImageUtils.convertYUV420ToImage(
      a["img"]!.cameraImage,
    );
    if (Platform.isAndroid) {
      image = image_lib.copyRotate(image, 90);
    }

    final classifier = Classifier(
        interpreter: Interpreter.fromAddress(
          a["img"]!.interpreterAddress,
        ),
        interpreter2: Interpreter.fromAddress(
          a["img"]!.interpreterAddress2,
        ));
    return classifier.predict(image, a["data"]);
  }
}

class IsolateData {
  IsolateData(
      {required this.cameraImage,
      required this.interpreterAddress,
      required this.interpreterAddress2});
  final CameraImage cameraImage;
  final int interpreterAddress;
  final int interpreterAddress2;
}
