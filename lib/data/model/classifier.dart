import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';
import 'dart:ui';
import 'package:flutter_yolov5_app/app.dart';
import 'package:flutter_yolov5_app/main.dart';
import 'package:ml_linalg/distance.dart';
import 'package:ml_linalg/linalg.dart';

import 'package:flutter/widgets.dart';
import 'package:image/image.dart' as image_lib;
import 'package:tflite_flutter/tflite_flutter.dart';
import 'package:tflite_flutter_helper/tflite_flutter_helper.dart';

import 'package:flutter_yolov5_app/utils/logger.dart';
import 'package:flutter_yolov5_app/data/entity/recognition.dart';

import 'dart:async';
import 'package:flutter/services.dart' show NetworkAssetBundle, rootBundle;
import 'package:path_provider/path_provider.dart';
import 'package:collection/collection.dart';

class Classifier {
  Classifier({Interpreter? interpreter, Interpreter? interpreter2}) {
    loadModel(interpreter);
    loadModel2(interpreter2);
  }
  late Interpreter? _interpreter;
  Interpreter? get interpreter => _interpreter;

  late Interpreter? _interpreter2;
  Interpreter? get interpreter2 => _interpreter2;

  static const String modelFileName = 'best-fp16-s-320.tflite';
  static const String modelFileName2 = 'split_v5_36.tflite';

  /// image size into interpreter
  static const int inputSize = 320;

  ImageProcessor? imageProcessor;
  late List<List<int>> _outputShapes = [];
  late List<TfLiteType> _outputTypes = [];

  static const int clsNum = 2;
  static const double objConfTh = 0.40;
  static const double clsConfTh = 0.70;
  static const double angleConfTh = 0.70;
  static double mNmsThresh = 0.6;
  double threshold = 0.5;

  late Map? _data;
  Map? get data => _data;

  // Map? data;

  List? input;
  List? output;

  /// load interpreter
  Future<void> loadModel(Interpreter? interpreter) async {
    try {
      _interpreter = interpreter ??
          await Interpreter.fromAsset(
            modelFileName,
            options: InterpreterOptions()..threads = 4,
          );
      final outputTensors = _interpreter!.getOutputTensors();
      _outputShapes = [];
      _outputTypes = [];
      for (final tensor in outputTensors) {
        _outputShapes.add(tensor.shape);
        _outputTypes.add(tensor.type);
      }
    } on Exception catch (e) {
      logger.warning(e.toString());
    }
  }

  Future<void> loadModel2(Interpreter? interpreter2) async {
    try {
      _interpreter2 = interpreter2 ??
          await Interpreter.fromAsset(modelFileName2,
              options: InterpreterOptions()..threads = 4);
    } on Exception {
      print('Failed to load model.');
    }
  }

  /// image pre process
  TensorImage getProcessedImage(TensorImage inputImage) {
    final padSize = max(inputImage.height, inputImage.width);

    imageProcessor ??= ImageProcessorBuilder()
        .add(
          ResizeWithCropOrPadOp(
            padSize,
            padSize,
          ),
        )
        .add(
          ResizeOp(
            inputSize,
            inputSize,
            ResizeMethod.BILINEAR,
          ),
        )
        .build();
    return imageProcessor!.process(inputImage);
  }

  Future<List<Recognition>> predict(image_lib.Image image, data) async {
    // print(MyApp.data);
    // print(interpreter2);
    // exit(0);
    var nowwww1 = new DateTime.now().millisecondsSinceEpoch;
    if (_interpreter == null) {
      return [];
    }
    var now4 = new DateTime.now().millisecondsSinceEpoch;
    var inputImage = TensorImage.fromImage(image);
    inputImage = getProcessedImage(inputImage);

    ///  normalize from zero to one
    List<double> normalizedInputImage = [];
    for (var pixel in inputImage.tensorBuffer.getDoubleList()) {
      normalizedInputImage.add(pixel / 255.0);
    }
    var normalizedTensorBuffer = TensorBuffer.createDynamic(TfLiteType.float32);
    normalizedTensorBuffer
        .loadList(normalizedInputImage, shape: [inputSize, inputSize, 3]);

    final inputs = [normalizedTensorBuffer.buffer];

    /// tensor for results of inference
    final outputLocations = TensorBufferFloat(_outputShapes[0]);
    final outputs = {
      0: outputLocations.buffer,
    };
    var now5 = new DateTime.now().millisecondsSinceEpoch;
    print("Time xu ly anh 2:${now5 - now4}");
    var now6 = new DateTime.now().millisecondsSinceEpoch;
    _interpreter!.runForMultipleInputs(inputs, outputs);
    var now7 = new DateTime.now().millisecondsSinceEpoch;
    print("Time xu ly model :${now7 - now6}");

    var now8 = new DateTime.now().millisecondsSinceEpoch;

    final ByteData imageData = await NetworkAssetBundle(Uri.parse(
            "https://res.cloudinary.com/ntbn/image/upload/v1642349037/Product/87_qokc6c.jpg"))
        .load("");
    final Uint8List bytes = imageData.buffer.asUint8List();
    var croppedImage = image_lib.decodeImage(bytes);
    croppedImage = image_lib.copyResize(croppedImage!,
        width: 128, height: 128, interpolation: image_lib.Interpolation.linear);
    List input = imageToByteListFloat32(croppedImage, 128);
    print(input);
    input = input.reshape([1, 3, 128, 128]);
    List output = List.filled(1 * 128, null, growable: false).reshape([1, 128]);
    _interpreter2!.run(input, output);
    output = output.reshape([128]);
    List<num> e1 = List.from(output);
    var e2 = [
      -0.08803766965866089,
      -0.039203524589538574,
      -0.4215824604034424,
      -9.882909378020055e-16,
      0.0,
      0.05848860740661621,
      0.15087652206420898,
      -3.88260534833762e-11,
      0.23537135124206543,
      0.585557222366333,
      0.05014967918395996,
      -9.62878402788192e-07,
      0.5437502861022949,
      -9.105497156269848e-07,
      -0.07225680351257324,
      0.6188607215881348,
      -0.028255939483642578,
      0.34837520122528076,
      -0.13871026039123535,
      -0.34058284759521484,
      0.02565741539001465,
      -0.13790559768676758,
      -0.42018699645996094,
      -0.05541229248046875,
      0.4012737274169922,
      -8.667893780511804e-08,
      -0.15516316890716553,
      -0.1118631362915039,
      -0.423492431640625,
      -0.3331122398376465,
      -0.1684410572052002,
      -1.813404981731992e-11,
      0.13949710130691528,
      -4.662048525005957e-10,
      0.5275115966796875,
      0.29953765869140625,
      -7.064212428453502e-17,
      -0.09012293815612793,
      0.0,
      -2.294822479598224e-06,
      -0.06866455078125,
      0.24603044986724854,
      0.18574190139770508,
      -2.4669134290888906e-08,
      -4.12139343097806e-06,
      -0.21160364151000977,
      -0.23975563049316406,
      0.18087804317474365,
      0.3609076738357544,
      0.14249050617218018,
      -0.07656800746917725,
      0.05097848176956177,
      0.04065442085266113,
      -1.4625948097091168e-07,
      -1.0939547792077065e-06,
      -0.3005896806716919,
      -0.16426563262939453,
      -0.1296229362487793,
      -0.042749881744384766,
      -0.05320751667022705,
      -0.513498067855835,
      0.0805826187133789,
      0.47412109375,
      -1.520202204119414e-06,
      -1.0105650289915502e-06,
      0.1502668857574463,
      0.0,
      0.7094714641571045,
      0.42488789558410645,
      -0.34733057022094727,
      -0.015427231788635254,
      -3.091809048783034e-07,
      0.701411247253418,
      0.0,
      -2.504457370378077e-06,
      -0.2672109603881836,
      -0.16383910179138184,
      -2.2436950075643836e-09,
      0.6612229347229004,
      0.21844768524169922,
      -9.577538762073345e-11,
      -2.3702311204780273e-17,
      -0.11107730865478516,
      -1.3110402505844831e-06,
      0.6327509880065918,
      0.3107481002807617,
      -0.44600677490234375,
      -0.4759197235107422,
      -1.4626857591792941e-06,
      -1.0254409313201904,
      0.07676219940185547,
      -0.14772415161132812,
      -2.8354329681639963e-21,
      -7.200924301287159e-08,
      0.0,
      -9.647491139297415e-13,
      -1.3290191418491304e-06,
      0.22221136093139648,
      -9.223731467500329e-07,
      -1.2506461644079536e-07,
      -9.940413292497396e-07,
      -0.33295488357543945,
      0.04590880870819092,
      -0.3114391565322876,
      -0.7050192356109619,
      -1.6037640810756323e-26,
      -0.4346160888671875,
      -0.12208271026611328,
      0.3018839359283447,
      0.0,
      -0.1334843635559082,
      -0.20905423164367676,
      -4.391709808260202e-06,
      -0.4572563171386719,
      -0.2998361587524414,
      0.05319070816040039,
      -0.09387290477752686,
      -2.405587122729369e-15,
      0.0,
      -0.26895618438720703,
      -0.17214560508728027,
      -1.0841822624206543,
      -0.21042275428771973,
      -0.2127152681350708,
      0.5402047634124756,
      -2.1096596691805303e-12,
      0.22663307189941406,
      0.0
    ];
    print(e1);
    // var result = "50";
    print(cosineDistanceBetweenVectors(e1, e2));
    // var result = compare(e1, data);
    // print(result);
    exit(0);

    /// make recognition
    final recognitions = <Recognition>[];
    // List<double> results = outputLocations.getDoubleList();
    // print(results.length);
    // exit(0);
    for (var i = 0; i < 1178100; i += (5 + clsNum + 180)) {
      if (outputLocations.getDoubleValue(i + 4) < objConfTh) continue;
      int cls = 0;
      double maxAngleConf = 0;
      int angle = 0;
      for (var j = i + 5 + clsNum; j < i + 5 + clsNum + 180; j++) {
        double angleconf = outputLocations.getDoubleValue(j);
        if (angleconf > maxAngleConf) {
          maxAngleConf = angleconf;
          angle = j - (i + 5 + clsNum);
        }
      }
      if (maxAngleConf < angleConfTh) continue;
      Rect outputRect = Rect.fromCenter(
        center: Offset(
          outputLocations.getDoubleValue(i) * inputSize,
          outputLocations.getDoubleValue(i + 1) * inputSize,
        ),
        width: outputLocations.getDoubleValue(i + 3) * inputSize,
        height: outputLocations.getDoubleValue(i + 2) * inputSize,
      );
      Rect transformRect = imageProcessor!
          .inverseTransformRect(outputRect, image.height, image.width);
      image_lib.PngEncoder pngEncoder =
          image_lib.PngEncoder(level: 0, filter: 0);
      List<int> png = pngEncoder.encodeImage(image);
      var imageTemp = Image.memory(Uint8List.fromList(png));
      recognitions.add(Recognition(i, "0", 1, transformRect, angle, imageTemp));
    }
    if (recognitions.isEmpty) {
      image_lib.PngEncoder pngEncoder =
          image_lib.PngEncoder(level: 0, filter: 0);
      List<int> png = pngEncoder.encodeImage(image);
      var imageTemp = Image.memory(Uint8List.fromList(png));
      recognitions.add(Recognition(
          -99, '0', 0, const Rect.fromLTRB(0, 0, 0, 0), 0, imageTemp));
    } else {
      List<Recognition> recognitionsNMS = nms(recognitions);
      for (var i = 0; i < recognitionsNMS.length; i++) {
        var x = recognitionsNMS[i].location.left;
        var y = recognitionsNMS[i].location.top;
        var w = recognitionsNMS[i].location.width;
        var h = recognitionsNMS[i].location.height;
        var angle = recognitionsNMS[i].angle;
        image_lib.Image croppedImage = image_lib.copyRotate(image, angle);
        var w_img = image.width;
        var h_img = image.height;
        var x_c = x + w / 2;
        var y_c = y + h / 2;
        var x_c1;
        var y_c1;
        var x1;
        var y1;
        var angle_rad;
        if (angle > 90) {
          angle_rad = (180 - angle.toDouble()) * pi / 180;
          x_c1 =
              croppedImage.width - x_c * cos(angle_rad) - y_c * sin(angle_rad);
          y_c1 = croppedImage.height -
              y_c * cos(angle_rad) -
              (w_img - x_c) * sin(angle_rad);
          x1 = x_c1 - w / 2;
          y1 = y_c1 - h / 2;
        } else {
          angle_rad = (90 - angle.toDouble()) * pi / 180;
          x_c1 = croppedImage.width -
              y_c * cos(angle_rad) -
              (w_img - x_c) * sin(angle_rad);
          y_c1 = croppedImage.height -
              (w_img - x_c) * cos(angle_rad) -
              (h_img - y_c) * sin(angle_rad);
          x1 = x_c1 - w / 2;
          y1 = y_c1 - h / 2;
        }

        croppedImage = image_lib.copyCrop(
            croppedImage, x1.round(), y1.round(), w.round(), h.round());

        croppedImage = image_lib.copyResize(croppedImage,
            width: 128,
            height: 128,
            interpolation: image_lib.Interpolation.linear);
        List input = imageToByteListFloat32(croppedImage, 128);
        input = input.reshape([1, 3, 128, 128]);
        List output =
            List.filled(1 * 128, null, growable: false).reshape([1, 128]);
        _interpreter2!.run(input, output);
        output = output.reshape([128]);
        List<num> e1 = List.from(output);
        // var result = "50";
        var result = compare(e1, data);
        // print(result);
        // exit(0);
        recognitionsNMS[i].setLabel = result!;
        image_lib.PngEncoder pngEncoder =
            image_lib.PngEncoder(level: 0, filter: 0);
        List<int> png = pngEncoder.encodeImage(croppedImage);
        var imageTemp = Image.memory(Uint8List.fromList(png));
        recognitionsNMS[i].setImage = imageTemp;
      }

      var now9 = new DateTime.now().millisecondsSinceEpoch;
      print("Time xu ly ketqua có box :${now9 - now8}");
      return recognitionsNMS;
    }

    var now10 = new DateTime.now().millisecondsSinceEpoch;
    print("Time xu ly ketqua no box :${now10 - now8}");
    var nowwww2 = new DateTime.now().millisecondsSinceEpoch;
    print("Time xu ly ketqua tongtest :${nowwww2 - nowwww1}");

    return recognitions;
  }

  Float32List imageToByteListFloat32(image_lib.Image image, int inputSize) {
    var convertedBytes = Float32List(1 * inputSize * inputSize * 3);
    var buffer = Float32List.view(convertedBytes.buffer);
    int pixelIndex = 0;
    for (var i = 0; i < inputSize; i++) {
      for (var j = 0; j < inputSize; j++) {
        var pixel = image.getPixel(j, i);
        buffer[pixelIndex++] = (image_lib.getRed(pixel) - 0 * 255) / 1;
        buffer[pixelIndex++] = (image_lib.getGreen(pixel) - 0 * 255) / 1;
        buffer[pixelIndex++] = (image_lib.getBlue(pixel) - 0 * 255) / 1;
      }
    }
    return convertedBytes.buffer.asFloat32List();
  }

  String? compare(List<num> currEmb, data) {
    double minDist = 99999;
    double curerDist = 0.0;
    String predRes = "NOT RECOGNIZED";
    for (String label in data!.keys) {
      print(label);
      for (var vector in data![label]) {
        curerDist = cosineDistanceBetweenVectors(currEmb, vector);
        if (curerDist <= threshold && curerDist < minDist) {
          minDist = curerDist;
          predRes = label;
        }
      }
      print(minDist);
    }
    print(minDist.toString() + " " + predRes);
    exit(0);
    return predRes;
  }

  double boxIou(Rect a, Rect b) {
    return boxIntersection(a, b) / boxUnion(a, b);
  }

  double boxIntersection(Rect a, Rect b) {
    double w = overlap((a.left + a.right) / 2, a.right - a.left,
        (b.left + b.right) / 2, b.right - b.left);
    double h = overlap((a.top + a.bottom) / 2, a.bottom - a.top,
        (b.top + b.bottom) / 2, b.bottom - b.top);
    if ((w < 0) || (h < 0)) {
      return 0;
    }
    double area = (w * h);
    return area;
  }

  double boxUnion(Rect a, Rect b) {
    double i = boxIntersection(a, b);
    double u = ((((a.right - a.left) * (a.bottom - a.top)) +
            ((b.right - b.left) * (b.bottom - b.top))) -
        i);
    return u;
  }

  double overlap(double x1, double w1, double x2, double w2) {
    double l1 = (x1 - (w1 / 2));
    double l2 = (x2 - (w2 / 2));
    double left = ((l1 > l2) ? l1 : l2);
    double r1 = (x1 + (w1 / 2));
    double r2 = (x2 + (w2 / 2));
    double right = ((r1 < r2) ? r1 : r2);
    return right - left;
  }

  // non-maximum suppression
  List<Recognition> nms(list) // Turned from Java's ArrayList to Dart's List.
  {
    List<Recognition> nmsList = <Recognition>[];

    // 1.find max confidence per class
    PriorityQueue<Recognition> pq = new HeapPriorityQueue<Recognition>();
    for (int i = 0; i < list.length; ++i) {
      pq.add(list[i]);
    }

    // 2.do non maximum suppression
    while (pq.length > 0) {
      // insert detection with max confidence
      List<Recognition> detections = pq.toList(); //In Java: pq.toArray(a)
      Recognition max = detections[0];
      nmsList.add(max);
      pq.clear();
      for (int j = 1; j < detections.length; j++) {
        Recognition detection = detections[j];
        Rect b = detection.location;
        if (boxIou(max.location, b) < mNmsThresh) {
          pq.add(detection);
        }
      }
    }

    return nmsList;
  }
}

double cosineDistanceBetweenVectors(List<num> e1, List e2) {
  List<double> e3 = e2.cast<double>();
  final vector1 = Vector.fromList(e1);
  final vector2 = Vector.fromList(e3);
  final result = vector1.distanceTo(vector2, distance: Distance.cosine);
  return result;
}
